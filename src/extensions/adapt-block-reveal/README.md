adapt-block-reveal
===============

An extension that allows a button or an image button to be added to a block which reveals a hidden block.


Installation
------------

First, be sure to install the [Adapt Command Line Interface](https://github.com/cajones/adapt-cli), then from the command line run:-

        adapt install adapt-block-popup



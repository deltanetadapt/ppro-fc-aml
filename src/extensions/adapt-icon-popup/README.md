adapt-icon-popup
===============

An extension that allows a button to be added to an article, block or component which opens more content in a popup.

